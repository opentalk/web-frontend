// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2
import { Collapse as MuiCollapse, Grid, Stack, styled, Tooltip, Typography } from '@mui/material';
import { isTimelessEvent } from '@opentalk/rest-api-rtk-query';
import { uniqueId } from 'lodash';
import { useTranslation } from 'react-i18next';

import { FavoriteIcon as OriginalFavoriteIcon } from '../../../assets/icons';
import EventTimePreview from '../../EventTimePreview';
import MeetingPopover, { MeetingCardFragmentProps } from './MeetingPopover';

const CardWrapper = styled('div')(({ theme }) => ({
  width: '100%',
  background: theme.palette.background.paper,
  borderRadius: theme.borderRadius ? theme.borderRadius.medium : 0,
  padding: theme.spacing(3),
  position: 'relative',
  [theme.breakpoints.up('sm')]: {
    '& .MuiTypography-root': {
      lineHeight: 1.1,
    },
  },
}));

const Collapse = styled(MuiCollapse)(({ theme }) => ({
  position: 'absolute',
  top: `-2%`,
  right: theme.spacing(6),
}));

const FavoriteIcon = styled(OriginalFavoriteIcon)(({ theme }) => ({
  width: 20,
  height: 20,

  [theme.breakpoints.down('md')]: {
    width: theme.typography.pxToRem(16),
    height: theme.typography.pxToRem(16),
  },
}));

const StandardCard = ({ event, isMeetingCreator, highlighted }: MeetingCardFragmentProps) => {
  const { t } = useTranslation();
  const { title, isFavorite } = event;
  const author = `${event.createdBy.firstname} ${event.createdBy.lastname}`;
  const isAllDay = !isTimelessEvent(event) ? !!event.isAllDay : false;
  const startDate = !isTimelessEvent(event) && event.startsAt ? new Date(event.startsAt.datetime) : undefined;
  const endDate = !isTimelessEvent(event) && event.endsAt ? new Date(event.endsAt.datetime) : undefined;
  const isTimeIndependent = !!event.isTimeIndependent;

  const renderTimeString = () => {
    let timeString = t('dashboard-meeting-card-error');

    if (isAllDay) {
      timeString = t('dashboard-meeting-card-all-day');
    } else if (isTimeIndependent) {
      timeString = t('dashboard-meeting-card-timeindependent');
    } else if (endDate && startDate) {
      return (
        <Typography variant="body2" noWrap>
          <EventTimePreview startDate={startDate} endDate={endDate} />
        </Typography>
      );
    }

    return (
      <Typography variant="body2" noWrap>
        {timeString}
      </Typography>
    );
  };

  const renderCreator = () => {
    const authorWithPrefix = isMeetingCreator ? `${author} (${t('global-me')})` : author;

    return (
      <Typography variant="body2" noWrap>
        {t('dashboard-home-created-by', { author: authorWithPrefix })}
      </Typography>
    );
  };

  return (
    <CardWrapper>
      <Collapse in={isFavorite} data-testid={`favorite-icon${isFavorite ? '-visible' : ''}`}>
        <FavoriteIcon type="functional" title={t('global-favorite')} titleId={uniqueId('favorite-icon-')} />
      </Collapse>
      <Grid
        container
        spacing={2}
        sx={{
          alignItems: 'flex-end',
          justifyContent: 'space-between',
        }}
      >
        <Grid item>
          <Stack spacing={2}>
            {renderTimeString()}

            <Tooltip translate="no" title={title || ''} describeChild placement="bottom-start">
              <Typography
                variant="h1"
                component="h3"
                noWrap
                sx={{
                  fontWeight: 600,
                  whiteSpace: 'normal',
                }}
              >
                {title}
              </Typography>
            </Tooltip>
            {renderCreator()}
          </Stack>
        </Grid>
        <Grid item>
          <MeetingPopover event={event} isMeetingCreator={isMeetingCreator} highlighted={highlighted} />
        </Grid>
      </Grid>
    </CardWrapper>
  );
};

export default StandardCard;
