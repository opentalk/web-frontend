// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2

export enum AccordionOptions {
  Rooms = 'rooms',
  Participants = 'participants',
  Groups = 'groups',
  Moderators = 'Moderators',
}
