// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2
import CloseMeetingDialog, { CloseMeetingDialogProps } from './CloseMeetingDialog';

export type { CloseMeetingDialogProps };
export default CloseMeetingDialog;
