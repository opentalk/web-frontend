// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2

export enum VoteType {
  Poll = 'Poll',
  LegalVote = 'LegalVote',
}

export const LEGEND_TITLE_ID = 'legend-title';
