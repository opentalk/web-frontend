// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2

export enum MenuTab {
  Chat = 'chat',
  People = 'people',
  Messages = 'messages',
}
