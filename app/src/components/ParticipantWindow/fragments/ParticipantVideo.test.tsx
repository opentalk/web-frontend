// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2
import { cleanup, screen } from '@testing-library/react';

import { idFromDescriptor } from '../../../modules/WebRTC';
import { VideoSetting } from '../../../types';
import {
  renderWithProviders,
  mockedVideoMediaDescriptor,
  mockStore,
  mockedParticipant,
} from '../../../utils/testUtils';
import ParticipantVideo from './ParticipantVideo';

jest.mock('@livekit/components-react', () => ({
  useParticipantContext: () => mockedParticipant(0),
  useRoomContext: () => jest.fn(),
}));

jest.mock('./RemoteVideo', () => ({
  __esModule: true,
  default: () => <div data-testid="remoteVideo"></div>,
}));
jest.mock('./ScreenPresenterVideo', () => ({
  __esModule: true,
  default: () => <div data-testid="screenPresenterVideo"></div>,
}));

const { store } = mockStore(1, { video: true, screen: true });
const participant = mockedParticipant(0);
const participantId = participant.id;

const ParticipantWindowProps = {
  quality: VideoSetting.Low,
  fullscreenMode: false,
  participantId: participantId,
  presenterVideoIsActive: true,
};

describe('ParticipantVideo', () => {
  afterEach(() => cleanup());

  //TODO UNIT TESTS rewrite the tests
  test('render participantVideo component', () => {
    renderWithProviders(<ParticipantVideo {...ParticipantWindowProps} />, { store, provider: { mui: true } });

    expect(screen.getByTestId('avatarContainer')).toBeInTheDocument();
  });

  test('render participantVideo component with video stream only', () => {
    const { store } = mockStore(1, { video: true, screen: false });
    const participant = mockedParticipant(0);
    renderWithProviders(<ParticipantVideo {...ParticipantWindowProps} participantId={participant.id} />, {
      store,
      provider: { mui: true },
    });

    expect(screen.queryByTestId('participantSreenShareVideo')).not.toBeInTheDocument();
  });

  test('render participantVideo component without any stream should only display avatar component', () => {
    const { store } = mockStore(1, { video: false, screen: false });
    const participant = mockedParticipant(0);
    renderWithProviders(<ParticipantVideo {...ParticipantWindowProps} participantId={participant.id} />, {
      store,
      provider: { mui: true },
    });

    expect(screen.getByTestId('avatarContainer')).toBeInTheDocument();
    expect(screen.queryByTestId('participantSreenShareVideo')).not.toBeInTheDocument();
    expect(
      screen.queryByTestId(`remoteVideo-${idFromDescriptor(mockedVideoMediaDescriptor(0))}`)
    ).not.toBeInTheDocument();
  });
});
