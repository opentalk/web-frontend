// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2
import AddIcon from './AddIcon';
import AddUserIcon from './AddUserIcon';
import ArrowDownIcon from './ArrowDownIcon';
import ArrowLeftIcon from './ArrowLeftIcon';
import ArrowRightIcon from './ArrowRightIcon';
import ArrowUpIcon from './ArrowUpIcon';
import AttendanceReportIcon from './AttendanceReportIcon';
import AudioIcon from './AudioIcon';
import AvatarIcon from './AvatarIcon';
import BackIcon from './BackIcon';
import BreakroomsIcon from './BreakroomsIcon';
import BugIcon from './BugIcon';
import BurgermenuIcon from './BurgermenuIcon';
import CameraOffIcon from './CameraOffIcon';
import CameraOnIcon from './CameraOnIcon';
import CheckIcon from './CheckIcon';
import ClockIcon from './ClockIcon';
import CloseIcon from './CloseIcon';
import CoffeeBreakIcon from './CoffeeBreakIcon';
import ConnectionBadIcon from './ConnectionBadIcon';
import ConnectionGoodIcon from './ConnectionGoodIcon';
import ConnectionMediumIcon from './ConnectionMediumIcon';
import CopyIcon from './CopyIcon';
import DashboardLegalIcon from './DashboardLegalIcon';
import DebriefingIcon from './DebriefingIcon';
import DoneIcon from './DoneIcon';
import DurationIcon from './DurationIcon';
import EditIcon from './EditIcon';
import EmojiIcon from './EmojiIcon';
import EncryptedMessagesIcon from './EncryptedMessagesIcon';
import EndCallIcon from './EndCallIcon';
import ErrorIcon from './ErrorIcon';
import ExtendToTabIcon from './ExtendToTabIcon';
import FavoriteIcon from './FavoriteIcon';
import FeedbackIcon from './FeedbackIcon';
import ForwardIcon from './ForwardIcon';
import FullscreenViewIcon from './FullscreenViewIcon';
import GongIcon from './GongIcon';
import GridViewIcon from './GridViewIcon';
import HelpIcon from './HelpIcon';
import HelpSquareIcon from './HelpSquareIcon';
import HiddenIcon from './HiddenIcon';
import HomeIcon from './HomeIcon';
import InfoIcon from './InfoIcon';
import InfoOutlinedIcon from './InfoOutlinedIcon';
import InviteIcon from './InviteIcon';
import LegalBallotIcon from './LegalBallotIcon';
import LiveIcon from './LiveIcon';
import LockedIcon from './LockedIcon';
import LogoGradientIcon from './LogoGradientIcon';
import LogoIcon from './LogoIcon';
import LogoSmallIcon from './LogoSmallIcon';
import MeetingNotesIcon from './MeetingNotesIcon';
import MeetingsIcon from './MeetingsIcon';
import MicOffIcon from './MicOffIcon';
import MicOnIcon from './MicOnIcon';
import ModeratorIcon from './ModeratorIcon';
import MoreIcon from './MoreIcon';
import MuteAllIcon from './MuteAllIcon';
import MyAccountIcon from './MyAccountIcon';
import NewMessageIcon from './NewMessageIcon';
import NoMessagesIcon from './NoMessagesIcon';
import NoOfParticipantsIcon from './NoOfParticipantsIcon';
import NoOfRoomsIcon from './NoOfRoomsIcon';
import NoPollsIcon from './NoPollsIcon';
import NoVotesIcon from './NoVotesIcon';
import PhoneIcon from './PhoneIcon';
import PictureIcon from './PictureIcon';
import PinIcon from './PinIcon';
import PollIcon from './PollIcon';
import RaiseHandOffIcon from './RaiseHandOffIcon';
import RaiseHandOnIcon from './RaiseHandOnIcon';
import RecordingsIcon from './RecordingsIcon';
import RectAddPlusIcon from './RectAddPlusIcon';
import RemoveIcon from './RemoveIcon';
import SearchIcon from './SearchIcon';
import SecureIcon from './SecureIcon';
import SendMessageIcon from './SendMessageIcon';
import SettingsIcon from './SettingsIcon';
import ShareScreenOffIcon from './ShareScreenOffIcon';
import ShareScreenOnIcon from './ShareScreenOnIcon';
import SharedFolderIcon from './SharedFolderIcon';
import SignOutIcon from './SignOutIcon';
import SortIcon from './SortIcon';
import SpeakerQueueIcon from './SpeakerQueueIcon';
import SpeakerViewIcon from './SpeakerViewIcon';
import SpeedTestIcon from './SpeedTestIcon';
import TalkingStickIcon from './TalkingStickIcon';
import TelephoneStrokeIcon from './TelephoneStrokeIcon';
import TimerIcon from './TimerIcon';
import TrashIcon from './TrashIcon';
import UnlockedIcon from './UnlockedIcon';
import VisibleIcon from './VisibleIcon';
import WarningIcon from './WarningIcon';
import WheelOfNamesIcon from './WheelOfNamesIcon';
import WhisperEmptyIcon from './WhisperEmptyIcon';
import WhisperFullIcon from './WhisperFullIcon';
import WhiteboardIcon from './WhiteboardIcon';
import WoolBallIcon from './WoolBallIcon';

export {
  AddIcon,
  AddUserIcon,
  ArrowDownIcon,
  ArrowLeftIcon,
  ArrowRightIcon,
  ArrowUpIcon,
  AttendanceReportIcon,
  AudioIcon,
  AvatarIcon,
  BackIcon,
  BreakroomsIcon,
  BugIcon,
  BurgermenuIcon,
  CameraOffIcon,
  CameraOnIcon,
  CheckIcon,
  ClockIcon,
  CloseIcon,
  CoffeeBreakIcon,
  ConnectionBadIcon,
  ConnectionGoodIcon,
  ConnectionMediumIcon,
  CopyIcon,
  DashboardLegalIcon,
  DebriefingIcon,
  DoneIcon,
  DurationIcon,
  EditIcon,
  EmojiIcon,
  EncryptedMessagesIcon,
  EndCallIcon,
  ErrorIcon,
  ExtendToTabIcon,
  FavoriteIcon,
  FeedbackIcon,
  ForwardIcon,
  FullscreenViewIcon,
  GongIcon,
  GridViewIcon,
  HelpIcon,
  HelpSquareIcon,
  HiddenIcon,
  HomeIcon,
  InfoIcon,
  InfoOutlinedIcon,
  InviteIcon,
  LegalBallotIcon,
  LiveIcon,
  LockedIcon,
  LogoGradientIcon,
  LogoIcon,
  LogoSmallIcon,
  MeetingNotesIcon,
  MeetingsIcon,
  MicOffIcon,
  MicOnIcon,
  ModeratorIcon,
  MoreIcon,
  MuteAllIcon,
  MyAccountIcon,
  NewMessageIcon,
  NoMessagesIcon,
  NoOfParticipantsIcon,
  NoOfRoomsIcon,
  NoPollsIcon,
  NoVotesIcon,
  PhoneIcon,
  PictureIcon,
  PinIcon,
  PollIcon,
  RaiseHandOffIcon,
  RaiseHandOnIcon,
  RecordingsIcon,
  RectAddPlusIcon,
  RemoveIcon,
  SearchIcon,
  SecureIcon,
  SendMessageIcon,
  SettingsIcon,
  SharedFolderIcon,
  ShareScreenOffIcon,
  ShareScreenOnIcon,
  SignOutIcon,
  SortIcon,
  SpeakerQueueIcon,
  SpeakerViewIcon,
  SpeedTestIcon,
  TalkingStickIcon,
  TelephoneStrokeIcon,
  TimerIcon,
  TrashIcon,
  UnlockedIcon,
  VisibleIcon,
  WarningIcon,
  WheelOfNamesIcon,
  WhisperEmptyIcon,
  WhisperFullIcon,
  WhiteboardIcon,
  WoolBallIcon,
};
