// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2
import CreateDirectMeeting from './CreateDirectMeeting';
import CreateEventsPage from './CreateEventsPage';
import EditEventsPage from './EditEventsPage';
import EventDetailsPage from './EventDetailsPage';
import EventsOverviewPage from './EventsOverviewPage';
import Home from './Home';
import SettingsAccountPage from './SettingsAccountPage';
import SettingsGeneralPage from './SettingsGeneral';
import SettingsProfilePage from './SettingsProfilePage';
import SettingsStoragePage from './SettingsStoragePage';
import UserManualPage from './UserManualPage';

export {
  SettingsAccountPage,
  SettingsGeneralPage,
  SettingsProfilePage,
  SettingsStoragePage,
  Home,
  CreateDirectMeeting,
  EventDetailsPage,
  EventsOverviewPage,
  EditEventsPage,
  CreateEventsPage,
  UserManualPage,
};
