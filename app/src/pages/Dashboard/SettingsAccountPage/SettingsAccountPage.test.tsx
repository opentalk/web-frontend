// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2
import { screen } from '@testing-library/react';

import { configureStore, renderWithProviders } from '../../../utils/testUtils';
import SettingsAccountPage from './SettingsAccountPage';

jest.mock('../../../api/rest', () => ({
  ...jest.requireActual('../../../api/rest'),
  useGetMeQuery: () => ({
    email: 'user@email.org',
    firstname: 'firstname',
    lastname: 'lastname',
  }),
}));

describe('SettingsAccountPage', () => {
  test('page will not crash', () => {
    const { store } = configureStore();
    renderWithProviders(<SettingsAccountPage />, { store });

    expect(screen.getByText('dashboard-settings-account-title')).toBeInTheDocument();
  });
});
