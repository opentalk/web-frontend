// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2

// show banner, when data size reaches this percentage of storage capacity
export const CRITICAL_STORAGE_CAPACITY_IN_PERCENT = 90;
export const STORAGE_SECTION_PATH = '/dashboard/settings/storage';
