// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2

export const LAST_SECONDS_OF_A_MINUTE = 5;
export const LAST_SECONDS_OF_TOTAL_TIME = 10;
