// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2
import ConsentNotification from './ConsentNotification';

export { showConsentNotification } from './utils';

export default ConsentNotification;
