// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2

// Here are explicitly keys, which we propagate to the parent element,
// keys that are not specified here we don't want to propagate because of the app shortkeys (like press-to-talk for instance)
export const KEYS_TO_PROPAGATE = ['Enter', 'Escape'];
